<?php

namespace kor3k\DateTime;

class DateTime extends \DateTime
{
    /**
     * @var DateTimeZone
     */
    private static $timeZone;
    
  
/**
 * 
 * @param string|\DateTime $time
 * @param \DateTimeZone $timeZone
 */    
    public function __construct( $time = 'now' , \DateTimeZone $timeZone = null )
    {		
        if( $time instanceof \DateTime )
        {
            $timeZone	=   $timeZone ?: $time->getTimezone();
            $time       =   $time->format('Y-m-d H:i:s');
//            $time	    =   '@'. $time->getTimestamp();
//            když to bylo přes timestamp, tak timezone name bylo +00:00...mimo...
        }

        $timeZone   =   $timeZone ? new DateTimeZone( $timeZone ) : static::getDefaultTimezone();

        parent::__construct( $time , $timeZone );
    }

    /**
     * @param int $unixTime
     * @return DateTime
     */
    public static function createFromUnixTime( $unixTime )
    {
        return new static( static::parseUnixTime( $unixTime ) );
    }

    /**
     * @param int $unixTime
     * @return bool|string mysql-like datetime
     */
    public static function parseUnixTime( $unixTime )
    {
        return date( 'Y-m-d H:i:s' , (int)$unixTime );
    }
    
    /**
     *
     * @return String
     */
    public function __toString() 
    {
        return $this->format( 'Y-m-d H:i:s' );
    }

/**
 * 
 * @param string|DateTimeZone $timezone
 */    
    public static function setDefaultTimezone( $timezone )
    {
        if( !( $timezone instanceof DateTimeZone ) )
        {
            $timezone	=   new DateTimeZone( $timezone );
        }
        self::$timeZone	=   $timezone;
        date_default_timezone_set( self::$timeZone->getName() );
    }

/**
 * 
 * @return DateTimeZone|null
 */    
    public static function getDefaultTimezone()
    {
	    return self::$timeZone ?: new DateTimeZone( date_default_timezone_get() );
    }



    /**
     * both start and end are included in the period
     *
     * @param \DateTime|string      $start
     * @param \DateTime|string      $end
     * @param \DateInterval|string  $interval
     * @return \DatePeriod
     */
    public static function createPeriod( $start , $end , $interval = 'P1D' )
    {
        return new \DatePeriod(
                                new static( $start ) ,
                                $interval instanceof \DateInterval ? $interval : new \DateInterval( $interval ) ,
                                new static( (string)$end ." +1 day" ) //include end date
                              );
    }

    /**
     * Return difference between $this and $time
     *
     * @param DateTime|String $time
     * @param boolean $absolute
     * @return \DateInterval
     */
    public function diff( $time = 'now' , $absolute = false ) 
    {
        if( !( $time instanceof \DateTime ) ) 
	{
            $time = new static( $time );
        }
	
        return parent::diff( $time , $absolute );
    }

    /**
     * Return Age in Years
     *
     * @param DateTime|String $time
     * @param boolean $absolute
     * @return Integer
     */
    public function getAgeYears( $time = 'now' , $absolute = true ) 
    {
        return $this->getAgeSeconds( $time , $absolute ) / 31536000;
    }    
    
    /**
     * Return Age in Minutes
     *
     * @param DateTime|String $time
     * @param boolean $absolute
     * @return Integer
     */
    public function getAgeMinutes( $time = 'now' , $absolute = true ) 
    {
        return $this->getAgeSeconds( $time , $absolute ) / 60;
    }
    
    /**
     * Return Age in Seconds
     *
     * @param DateTime|String $time
     * @param boolean $absolute
     * @return Integer
     */
    public function getAgeSeconds( $time = 'now' , $absolute = true ) 
    {	
	    $interval   =   $this->diff( $time , $absolute );
        
        $seconds    =   $interval->s;
        $seconds    +=  $interval->i * 60;
        $seconds    +=  $interval->h * 3600;
        $seconds    +=  $interval->d * 86400;
        $seconds    +=  $interval->m * 2628000;
        $seconds    +=  $interval->y * 31536000;
        
        return $seconds;
    }    
    
    /**
     * Return Age in Hours
     *
     * @param DateTime|String $time
     * @param boolean $absolute
     * @return Integer
     */
    public function getAgeHours( $time = 'now' , $absolute = true ) 
    {
	    return $this->getAgeSeconds( $time , $absolute ) / 3600;
    }     
    
    /**
     * Return Age in Days
     *
     * @param DateTime|String $time
     * @param boolean $absolute
     * @return Integer
     */
    public function getAgeDays( $time = 'now' , $absolute = true ) 
    {
	    return $this->getAgeSeconds( $time , $absolute ) / 86400;
    }    
    
    /**
     * Return Age in Months
     *
     * @param DateTime|String $time
     * @param boolean $absolute
     * @return Integer
     */
    public function getAgeMonths( $time = 'now' , $absolute = true ) 
    {
	    return $this->getAgeSeconds( $time , $absolute ) / 2628000;
    }

    /**
     * @param int $tresholdHour
     * @param int $daysAhead
     * @return static
     */
    public static function getFirstValidDate( $tresholdHour , $daysAhead )
    {
        if( (int)( new DateTime() )->format( 'H' ) >= (int)$tresholdHour )
        {
            $firstValidDate =   new static( "now +". (int)($daysAhead+1) ." day" );
        }
        else
        {
            $firstValidDate =   new static( "now +". (int)$daysAhead ." day" );
        }

        $firstValidDate->setTime( 0 , 0 , 0 );

        return $firstValidDate;
    }

    /**
     * @inheritdoc
     */
    public static function createFromFormat( $format , $time , $timezone = null )
    {
        $dt =   parent::createFromFormat( $format , $time , $timezone );

        if( !$dt )
        {
            return false;
        }
        else
        {
            return new static( $dt );
        }
    }

}
