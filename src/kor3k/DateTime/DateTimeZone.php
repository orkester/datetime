<?php

namespace kor3k\DateTime;

class DateTimeZone extends \DateTimeZone
{
    /**
     * @param string|\DateTimeZone $timezone
     */
    public function __construct( $timezone )
    {
        if( $timezone instanceof \DateTimeZone )
        {
            $timezone   =   $timezone->getName();
        }

        parent::__construct( $timezone );
    }
/**
 * 
 * @return string
 */    
    public function __toString()
    {
	    return $this->getName();
    }
}